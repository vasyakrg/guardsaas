function codeHex2Txt( instr ) {
    var codeHex = instr.toUpperCase();
    var l = codeHex.length;
    var codeHex = instr.substring(7, l-2);
    var l = codeHex.length;
    var codeP1 = codeHex.substring(0,l-4);
    var codeP2 = codeHex.substring(l-4);
    var codeP1I = parseInt(codeP1,16);
    var codeP2I = parseInt(codeP2,16);

    var codeText = ("000" + codeP1I).slice(-3) + ',' + ("00000" + codeP2I).slice(-5);
    return codeText;
}

function codeHex2Dec( instr ) {
    var codeHex = instr.toUpperCase();
    var codeInt = parseInt(codeHex,16);
    var codeDec = ("0000000000" + codeInt).slice(-10);
    return codeDec;
}

function copyToClipboard(instr){
    var copytext=document.createElement('input');
    if(typeof instr=='undefined') copytext.value=window.location.href;
    else copytext.value=instr;
    document.body.appendChild(copytext);
    copytext.select();
    document.execCommand('copy');
    document.body.removeChild(copytext);
}

function pasteFromClipboard() {
    navigator.permissions.query({
        name: 'clipboard-read'
    });
    navigator.clipboard.readText()
        .then(text => {
            document.getElementById("code_hex1").value = text;
        })
}

function convertIt1() {
    var codeHex = document.getElementById("code_hex1").value;
    codeHex = codeHex.replace(/\s+/g, '');
    document.getElementById("code_hex1").value = codeHex;

    document.getElementById("code_dec1").value = codeHex2Dec(codeHex);
    // document.getElementById("code_txt1").value = codeHex2Txt(codeHex);
    document.getElementById("code_txt1").value = 'Copied!';
    copyToClipboard(codeHex2Txt(codeHex));
    setTimeout(() => {
        document.getElementById("code_txt1").value = codeHex2Txt(codeHex);
    }, 1500);
}

